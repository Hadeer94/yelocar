package com.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.car.model.controller.CarController;

@SpringBootApplication
public class Main {

	public static void main(String[] args) {
	      SpringApplication.run( Main.class, args );

	}

}
