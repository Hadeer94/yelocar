package com.car.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.car.model.Enums.Color;
import com.car.model.Enums.HideMe;

@Entity
@Table(name="CAR")
public class Car {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name" , nullable=false)
	private String name;

	@Column(name = "color", nullable=false)
	@Enumerated(EnumType.STRING)
	private Color color;

	@Column(name = "model", nullable=false)
	private String model;
	
	@Column(name = "owner", nullable=false)
	private String owner;
	
	@Column(name = "hideMe" , nullable=false)
	private HideMe hideMe;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public HideMe getHideMe() {
		return hideMe;
	}

	public void setHideMe(HideMe hideMe) {
		this.hideMe = hideMe;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + ", color=" + color + ", model=" + model + ", owner=" + owner
				+ ", hideMe=" + hideMe + "]";
	}
	
	
	
	

}
