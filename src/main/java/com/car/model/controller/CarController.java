package com.car.model.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.car.model.Car;
import com.car.model.service.CarService;

@RestController
@RequestMapping("/cars")
@EnableAutoConfiguration
public class CarController {
	
	@Autowired
	CarService carService;
	
	  @GetMapping
	    public ResponseEntity<List<Car>> getAllCars() {
	        List<Car> list = carService.getAllCars();
	        return new ResponseEntity<List<Car>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	  
	  @GetMapping("/{id}")
	    public Car getCarById(@PathVariable("id") Long id) throws Exception {
	        Car car = carService.getCarById(id);
	        return car;
	    }
	  
	  @GetMapping("/user")
	    public ResponseEntity<List<Car>> getAllCarsForUsers() {
	        List<Car> list = carService.getAllCarsForUsers();
	        return new ResponseEntity<List<Car>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	  
	  @GetMapping("/byName")
	    public ResponseEntity<List<Car>> getAllCarsByName(){
	        List<Car> list = carService.getAllCarsByNameAsc();
	        return new ResponseEntity<List<Car>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	  
	  @GetMapping("/byOrder")
	    public ResponseEntity<List<Car>> getAllCarsByOrder(){
	        List<Car> list = carService.getAllCarsByModelAsc();
	        return new ResponseEntity<List<Car>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	  
	  @GetMapping("/name={name}")
	    public ResponseEntity<List<Car>> getCarsByName(@PathVariable("name") String name){
	        List<Car> list = carService.getCarsByName(name);
	        return new ResponseEntity<List<Car>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	  
	  @GetMapping("/owner={owner}")
	    public ResponseEntity<List<Car>> getCarsByOwner(@PathVariable("owner") String owner){
	        List<Car> list = carService.getCarsByOwner(owner);
	        return new ResponseEntity<List<Car>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	  
	  @PostMapping
	    public String insertCar(@RequestBody Car car) {
	        Car insertedCar=  carService.insertCar(car);
	        if(insertedCar != null)
	        	return "inserted";
	        return "error in inserting new car.";
	    }
	  
	  @DeleteMapping("/{id}")
	    public String deleteCarById(@PathVariable("id") Long id) throws Exception {
	        carService.deleteCar(id);
	        return "Deleted";
	    }
	  
	  
	  @PutMapping("/{id}")
	    public String insertCar(@PathVariable("id") Long id,@RequestBody Car car) throws Exception {
	        Car insertedCar=  carService.updateCar(id,car);
	        if(insertedCar != null)
	        	return "updated";
	        return "error in updating new car.";
	    }
	  
	 
}
