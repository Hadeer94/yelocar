package com.car.model.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.car.model.Car;
import com.car.model.Enums.HideMe;
import com.car.model.repo.CarsRepository;

@Service
public class CarService {
	
	@Autowired
	CarsRepository carsRepository;
	
	   public List<Car> getAllCars()
	    {
	        List<Car> carList = carsRepository.findAll();	         
	        if(carList.size() > 0) {
	            return carList;
	        } else {
	            return null;
	        }
	    }
	   
	   public List<Car> getAllCarsForUsers()
	    {
	        List<Car> carList = carsRepository.findAll();	         
	        if(carList.size() > 0) {
	            return carList.stream().filter(c->c.getHideMe() == HideMe.SHOW).collect(Collectors.toList());
	        } else {
	            return null;
	        }
	    }

	   public Car getCarById(Long id) throws Exception  {
	        Optional<Car> car = carsRepository.findById(id);
	         
	        if(car.isPresent()) {
	            return car.get();
	        } else {
	            throw new Exception("No car record exist for given id");
	        }
	    }
	   
	   public List<Car> getAllCarsByNameAsc(){
	        List<Car> carList = carsRepository.findAllByOrderByNameAsc();	         
	        if(carList.size() > 0) {
	            return carList;
	        } else {
	            return null;
	        }
	    } 
	     
	    public Car insertCar(Car entity) {
	  	  	if(entity.getHideMe()==null)
	  	  		entity.setHideMe(HideMe.SHOW);
	        entity = carsRepository.save(entity);
	        return entity;
	  
	    } 
	    
	     
	    public void deleteCar(Long id) {
	    	Optional<Car> car = carsRepository.findById(id);
	    	Car fetchedCar = null;
	    	if(car.isPresent()){
	    		fetchedCar = car.get();
	    	}
	          carsRepository.delete(fetchedCar);
	    } 
	    
	     
	    

	    public void deleteCarById(Long id) throws Exception
	    {
	        Optional<Car> car = carsRepository.findById(id);
	         
	        if(car.isPresent()) 
	        {
	            carsRepository.deleteById(id);
	        } else {
	            throw new Exception("No Car record exist for given id");
	        }
	    }

		public Car updateCar(Long id, Car car) throws Exception {
			 Optional<Car> fetchedCar = carsRepository.findById(id);
			 Car updatedCar = null;
	         
		        if(fetchedCar.isPresent()) {
		        	updatedCar = fetchedCar.get();
		        	updatedCar.setColor(car.getColor()!=null? car.getColor() : updatedCar.getColor());
		        	updatedCar.setModel(car.getModel()!=null? car.getModel() : updatedCar.getModel());
		        	updatedCar.setName(car.getName()!=null? car.getName() : updatedCar.getName());
		        	updatedCar.setOwner(car.getOwner()!=null ? car.getOwner() : updatedCar.getOwner());
		        	updatedCar.setHideMe(car.getHideMe() != null ? car.getHideMe() : updatedCar.getHideMe());
		        	
		        	return carsRepository.save(updatedCar);
		        }
		        
		        else {
		            throw new Exception("No Car record exist for given id");
		        }
		}

		public List<Car> getAllCarsByModelAsc() {
			 List<Car> carList = carsRepository.findAllByOrderByModelAsc();	         
		        if(carList.size() > 0) {
		            return carList;
		        } else {
		            return null;
		        }
		}
		
		
		public List<Car> getCarsByName(String name) {
			 List<Car> carList = carsRepository.findByNameContaining(name);	         
		        if(carList.size() > 0) {
		            return carList;
		        } else {
		            return null;
		        }
		}
		
		public List<Car> getCarsByOwner(String owner) {
			 List<Car> carList = carsRepository.findByOwnerContaining(owner);	         
		        if(carList.size() > 0) {
		            return carList;
		        } else {
		            return null;
		        }
		}
	}