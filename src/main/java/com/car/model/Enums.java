package com.car.model;

public class Enums {
	
	public enum HideMe 
    { 
        SHOW,HIDE;
    } 
	
	public enum Color 
	{
		RED("red"),
		YELLOW("yellow"),
		BLACK("black"),
		BROWN("brown"),
		BLUE("blue"),
		GREEN("green");

	 
	    private String color;
	 
	    Color(String color) {
	        this.color = color;
	    }
	 
	    public String getColor() {
	        return color;
	    }
	}
  

}
