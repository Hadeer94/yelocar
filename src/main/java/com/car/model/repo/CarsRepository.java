package com.car.model.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.car.model.Car;

@Repository
public interface CarsRepository extends JpaRepository<Car, Long> {
    public List<Car> findAllByOrderByNameAsc();
    public List<Car> findAllByOrderByModelAsc();
    public List<Car> findByNameContaining(String name);
    public List<Car> findByOwnerContaining(String owner);
}
